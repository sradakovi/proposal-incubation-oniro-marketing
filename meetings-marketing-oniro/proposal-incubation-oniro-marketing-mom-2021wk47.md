# Oniro WG Incubation stage marketing

Chair: Agustin
Scriba: Agustin

## Participants

Agustin, Chiara, Shanda, Clark, Yves, Davide, Gianluca, Ebba, Aurore, Dony, 
Alessandra, Patrick and Carlo

## Agenda
* Oniro launch impact - Shanda 10 min
* Tasks that we should accomplish until the WG is formed - Agustin 10 min
* Gitlab repo for marketing activities and mailing list - Agustin 5 min
* Evaluation of the marketing session at Solda. - Agustin 10 min
* Marketing Plan - Agustin 5 min
* Events from the lasts days - Agustin 5 min

## MoM

### Oniro launch impact - Shanda 10 min

Shanda went over the launch numbers. Media coverage above average the launch of 
other WG, about double.

### Evaluation of the marketing session at Solda. - Agustin 10 min

No time to cover this.

### Tasks that we should accomplish until the WG is formed - Agustin 10 min

What do we need to support the creation of the WG

* Value proposition (deck). We should create this collectively. Who will be 
leading this action? #task Davide will take the first step.
   * Davide and Yves 
suggest to appoint somebody that is not Eclipse or Huawei. 
   * Task proposed to Gianluca? Sold. The idea is to convince first SoCs. #task 
Coordinate the Value prop deliverable.
   * Value prop. group. #task Chiara will send the mail address for those 
involved.
* Pipeline will come afterwards. Positioning then

Can the EF provide examples of Marketing pick decks from other WG? #task Clark 
will provide.

Marketing content and communication

* Design the content we want to develop.
* How are we going to communicate at 
Oniro? Social media and other guidelines.
   * EF has guidelines to start with. #task Shanda will share them.
* Agustin will lead the definition of the guidelines. Input first then meeting 
to discuss.

Events list for 2022

* It is a priority for companies since they are planning next year.
   * #task Chiara will start by sending Huawei's list.
   * Then the rest, including EF. From there we will curate the list.


### Gitlab repo for marketing activities and mailing list - Agustin 5 min

Agustin will send a mail with the links.

### Marketing Plan - Agustin 5 min

No time to cover this.

### Events from the lasts days - Agustin 5 min

No time to cover this.
