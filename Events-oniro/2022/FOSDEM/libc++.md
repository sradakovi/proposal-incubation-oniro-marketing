# Libc++ on Linux - using the example of Oniro

## Track
LLVM

https://lists.llvm.org/pipermail/llvm-dev/2021-December/154215.html

## Title
Libc++ on Linux - using the example of Oniro

## Subtitle
Experiences with switching from libstdc++ to libc++

## Event type
Lightning Talk (5 min)

## Persons
Bernhard "bero" Rosenkränzer

Bero has been a Linux developer for 25 years - working on
desktop, server and embedded alike.
He is a regular speaker at Open Source conferences like
FOSDEM, Embedded Linux Conference, Linux Piter and Open
Source Summit.
He currently works on the Eclipse Foundation's Oniro
operating system. Previous employers include Linaro
and Red Hat.

## Abstract (one paragraph)
Oniro - the Eclipse Foundation's embedded operating system -
is switching from libstdc++ to libc++ by default. This talk
gives an overview of our experience daring to
make the switch.

## Description
Oniro - the Eclipse Foundation's embedded operating system -
is switching from libstdc++ to libc++ by default.

Binary compatibility is a holdup for libc++ adoption on
Linux desktops and servers - but in the embedded world,
we have more options.

This talk gives an overview of our experience daring to
make the switch.
