# Running an OpenThread Mesh Network with Linux and Zephyr

## Track
Embedded, Mobile and Automotive devroom

https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/#

## Title
Running an OpenThread Mesh Network with Linux and Zephyr

## Subtitle
Transparent IoT Gatway Blueprint of the Oniro Project

## Event type
20 min talk + 5 min QA

## Persons
Stefan Schmidt

Stefan Schmidt is a FOSS contributor for 16+ years now. During this time he
worked on different projects and different layers of the Linux ecosystem. From
bootloader and kernel over build systems for embedded to user interfaces. He was
serving as a technical steering committee member of OpenEmbedded during the
merge with the Yocto project, helped porting a 2.6 kernel to some early
smartphones, and was the release manager of the Enlightenment Foundation
Libraries. He currently co-maintains the Linux IEEE 802.15.4 subsystem.

After many years as a freelancer and long-time member of the Samsung Open Source
Group he recently joined Huawei's newly founded Open Source Technology Center as
a Principal Solutions Architect. Stefan is a regular speaker at open source related
conferences.

## Abstract
The Thread protocol specifies a low-power IoT mesh network. It offers
self-healing, low latency and IPv6 connectivity without a single point of
failure. In addition to the lower layer mesh functionality it also
offers mesh network management, as well as secure onboarding of headless
devices.

OpenThread is an open source project implementing the Thread protocol and its
components. The focus of this talk is to demonstrate a Linux based OpenThread
border router and Zephyr based mesh nodes. Tight together by a Yocto based
build system this talk shows all components you need to have an IPv6 enabled
micro-processor on a low-power wireless link. The used power is small enough
to allow operating a small sensor for months or years on a coin cell battery in such a
scenario. All served by a Linux based border router to allow for internet
access and end-to-end IPv6 connectivity.

All of the above is bundled together in an Eclipse Oniro Project blueprint
for a transparent IoT gateway.

## Links
Blueprint documentaion:
https://docs.oniroproject.org/projects/blueprints/en/latest/transparent-gateway.html

Demonstration video:
https://www.youtube.com/watch?v=o_3ITbSAvNg

Eclipse Oniro Project:
https://oniroproject.org
