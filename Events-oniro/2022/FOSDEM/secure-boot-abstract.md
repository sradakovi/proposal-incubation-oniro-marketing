# Secure boot et al

## Track
Hardware-Aided Trusted Computing devroom

https://lists.fosdem.org/pipermail/fosdem/2021q4/003334.html

## Title
Secure boot, TEEs, different OSes and more

## Subtitle
Making sense of the trusted computing landscape in Eclipse Oniro embedded
distribution

## Event type
20 min talk + 5 min QA

## Persons
Marta Rybczynska

Marta Rybczynska has network security background, 20 years of experience
in Open Source including 15 in embedded development. She has been working
with embedded operating systems like Linux and various real-time ones,
system libraries and frameworks up to user interfaces. Her specialties
are architecture-specific parts of the Linux kernel. In the past, Marta
served as Vice-President and treasurer for KDE e.V. She is involved in
various Open Source projects, and also contributing kernel-related guest
articles for LWN.net. Since early 2021, she has been contributing to the
Eclipse Oniro project. She has experience with presentations on both scientific
and free software conferences, including LinuxCon, Open Source Summit,
Embedded Linux Conference, Akademy and FOSDEM.

## Abstract (one paragraph)

In this talk Marta is going to present a map of the trusted computing
landscape, explaining different types hardware support. She is going
to put it in a context of implementing secure boot and trusted execution
in an embedded distribution, namely Yocto-based Eclipse Oniro project.

## Description

The trusted computing landscape could be hard to understand for newcomers.
Just at the beginning, they encounter a number of abbreviations like TEE,
OPTEE, SEV, TF-A, TF-M and many more.

In this talk Marta is going to present a map of those technologies, illustrate
how they are (or are expected to) be used, which market needs they address.
She will show how they could be implemented in practice in an embedded
distribution. The example will be the secure boot work in the Eclipse Oniro
project, an embedded multi-OS distribution for Internet of Things (IOT)
devices. The multi-OS specificity of Oniro will be used how the trusted
computing technologies compare on different types of processors running
Linux and Zephyr, with different security hardware support.

## Links
A previous presentation covering the more general topic of security in the
distribution at Embedded Linux Conference 2021
https://www.youtube.com/watch?v=u6Xo0QF6AMQ

Oniro project and its security tooling is available from
https://booting.oniroproject.org/
